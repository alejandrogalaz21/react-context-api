import React, { Component } from 'react'

// first we will make a new context
const MyContext = React.createContext()

// Then create a provider component
class MyProvider extends Component {
  state = {
    name: 'alex',
    age: 29,
    cool: true
  }

  changeName = name => this.setState({ name })

  growYearOlder = () => this.setState({ age: this.state.age + 1 })

  onChange = event => this.setState({ [event.target.name]: event.target.value })

  render() {
    return (
      <MyContext.Provider value={{ ...this.state, ...this }}>
        {this.props.children}
      </MyContext.Provider>
    )
  }
}

const House = props => (<div> Here Lives <Person /></div>)

class Person extends Component {
  render() {
    return (
      <MyContext.Consumer>
        {context => (
          <React.Fragment>
            <h1>I'm {context.state.name}</h1>
            <h1>And I'm {context.state.age} old</h1>
            <button onClick={context.growYearOlder}>Birth day</button>
            <button onClick={() => context.changeName('jesus')}>Change the name</button>
            <input type="text" name="name" value={context.state.name} onChange={context.onChange} />
          </React.Fragment>
        )}
      </MyContext.Consumer>
    )
  }
}

class App extends Component {
  render() {
    return (
      <MyProvider>
        <div className="">
          React.js Context API
        <House />
        </div>
      </MyProvider>
    )
  }
}

export default App